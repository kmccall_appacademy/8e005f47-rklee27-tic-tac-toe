class Board
  attr_accessor :grid

  def initialize(grid = nil)
    if grid == nil
      @grid = Array.new(3) { Array.new(3) }
    else
      @grid = grid
    end
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    return row_win if !row_win.nil? && row_win.length == 1
    return column_win if !column_win.nil? && column_win.length == 1
    return diagonal_win if !diagonal_win.nil? && diagonal_win.length == 1
  end

  def over?
    if winner
      return true
    elsif grid.flatten.uniq.nil?
      return false
    elsif grid.flatten.uniq.length == 2 && grid.flatten.length == 9
      return true
    end
  end

  def row_win
    grid.each do |row|
      if row.uniq.length == 1 && row.empty? != true
        return row.first
      end
    end
  end

  def diagonal_win
    left_diagonal = [self.grid[0][0], self.grid[1][1], self.grid[2][2]].uniq
    right_diagonal = [self.grid[0][2], self.grid[1][1], self.grid[2][0]].uniq
    return left_diagonal.first if left_diagonal.length == 1 &&
      !left_diagonal.include?(nil)
    return right_diagonal.first if right_diagonal.length == 1 &&
      !right_diagonal.include?(nil)
  end

  def column_win
    left_column = [self.grid[0][0], self.grid[1][0], self.grid[2][0]].uniq
    mid_column = [self.grid[0][1], self.grid[1][1], self.grid[2][1]].uniq
    right_column = [self.grid[0][2], self.grid[1][2], self.grid[2][2]].uniq
    return left_column.first if left_column.length == 1 &&
      !left_column.include?(nil)
    return mid_column.first if mid_column.length == 1 &&
      !mid_column.include?(nil)
    return right_column.first if right_column.length == 1 &&
      !right_column.include?(nil)
  end

  def [](pos)
    row, column = pos
    grid[row][column]
  end

  def []=(pos, mark)
    row, column = pos
    grid[row][column] = mark
  end
end
