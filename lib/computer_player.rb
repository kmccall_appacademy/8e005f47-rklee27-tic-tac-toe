class ComputerPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def board
    @board
  end

  def display(board)
    @board = board
  end

  def get_move
    possible_moves.each do |move|
      if winning_move?(move)
        return move
      else
        board.place_mark(move, nil)
      end
    end

    random_move
  end

  def random_move
    possible_moves.shuffle.first
  end

  def possible_moves
    possible_moves = []
    (0..2).each do |row|
      (0..2).each do |column|
        possible_moves << [row, column] if board.empty?([row, column])
      end
    end

    possible_moves
  end

  def winning_move?(move)
    board.place_mark(move, :O)
    board.winner == :O
  end

  def mark=(mark)
    mark
  end
end
