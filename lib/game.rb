require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :current_player, :board
  attr_reader :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    player_one.mark = :X
    player_two.mark = :O
    @current_player = player_one
    @board = Board.new
  end

  def play_turn
    move = current_player.get_move
    board.place_mark(move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def play
    current_player.display(board)
    play_turn while !board.over?
    win_message
  end

  def win_message
    if board.winner.nil?
      puts "TIE GAME"
    else
      switch_players!
      puts "#{current_player.name.upcase} WINS!"
    end
  end

  def switch_players!
    if current_player == player_one
      self.current_player = player_two
    else
      self.current_player = player_one
    end
  end
end
