class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where do you want to make your move?"
    move = gets.chomp
    pos = []
    move.split.each { |num| pos << num.to_i }
    pos
  end

  def display(board)
    puts board.grid
  end
end
